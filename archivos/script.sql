/* 
 Script de la Base de datos del blog de Tayka
 */
create database blog_tayka;
drop database blog_tayka;
use blog_tayka;
create table Usuario(
  id int AUTO_INCREMENT primary key,
  nombre varchar(50),
  passwd varchar(50),
  tipo varchar(40),
  email varchar(30)
);
create table Post(
  id int AUTO_INCREMENT primary key,
  content varchar(1200),
  imagen longblob
);
create table Comentario(
  id int AUTO_INCREMENT primary key,
  content varchar(200)
);